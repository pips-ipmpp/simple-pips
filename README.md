# SIMPLE-PIPS

The SIMPLE-PIPS model is specifically tailored for the usage with the parallel interior point solver PIPS-IPM++.
In addition to minor changes in the model structure compared to the other SIMPLE models, it implements various model 
annotations and several ways to generate the model in a format readable by the GAMS/PIPS-IPM++-Link.

1. [The SIMPLE model](#the-simple-model)
    1. [SIMPLE-PIPS parameters](#simple-pips-parameters)
    2. [Building the annotated gdx file](#building-the-annotated-gdx-file)
    3. [Solving the problem with PIPS-IPM++](#solving-the-problem-with-pips-ipm)
    4. [Block-wise generation of the gdx files](#block-wise-generation-of-the-gdx-files)
2. [Authors](#authors)
3. [Acknowledgements](#acknowledgements)


## The SIMPLE model

The SIMPLE model is a highly simplified representation of an optimizing energy system model (ESM) developed during
the BEAM-ME project. The model adressess the need to have a common ground to discuss speed-up methods for complex
ESMs. This is achieved by preserving relevant parts of the model structure that can be found in many complex ESMs 
but at the same time having a compact and comprehensive source code.

All SIMPLE models come with a data generator simple\_data\_ gen.gms that can be parametrized to generate data instances
of different size. The data generator is called automatically from the main model and takes only the number of regions
as an argument to scale the problem size. All the data is computed by randomizing standard basic time series that 
provide the corresponding data for a model region.

Data sources:
- **timeSeries.csv**
  - demand: [Actual electricity consumption in Germany 2019](https://www.smard.de/en/downloadcenter/download_market_data/5730#!?downloadAttributes=%7B%22selectedCategory%22:2,%22selectedSubCategory%22:5,%22selectedRegion%22:%22DE%22,%22from%22:1546297200000,%22to%22:1577832359999,%22selectedFileType%22:%22CSV%22%7D) 
    by [Bundesnetzagentur | SMARD.de](https://www.smard.de/) licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)
  - photovoltaic: [Actual electricity generation in Germany 2019](https://www.smard.de/en/downloadcenter/download_market_data/5730#!?downloadAttributes=%7B%22selectedCategory%22:1,%22selectedSubCategory%22:1,%22selectedRegion%22:%22DE%22,%22from%22:1546297200000,%22to%22:1577832359999,%22selectedFileType%22:%22CSV%22%7D) 
    by [Bundesnetzagentur | SMARD.de](https://www.smard.de/) licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)
  - wind: [Actual electricity generation in Germany 2019](https://www.smard.de/en/downloadcenter/download_market_data/5730#!?downloadAttributes=%7B%22selectedCategory%22:1,%22selectedSubCategory%22:1,%22selectedRegion%22:%22DE%22,%22from%22:1546297200000,%22to%22:1577832359999,%22selectedFileType%22:%22CSV%22%7D) 
    by [Bundesnetzagentur | SMARD.de](https://www.smard.de/) licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)
  - ecars: Mathematical approximation of a daily profile f(t) = 0.3 * sin( (t - 55) / 17.3 ) + 0.2 * sin( (t - 30.2) / 7 ) + 0.48

All other data are rough estimations based on the general range of technoeconomical data found in energy system models.

For a comprehensive explanation of all SIMPLE models and corresponding options head over to the 
[best practice guide](https://gitlab.com/beam-me/bpg/-/blob/master/EnergySystemModel_SpeedUp_BestPracticGuide.pdf).

## SIMPLE-PIPS parameters
SIMPLE-PIPS (simple4pips.gms) can be run from the command line as follows:
```bash
gams simple4pips.gms [optional parameters]
```

### Generic parameters for controlling the problem size
| Parameter  | Description                                                          | Range         | Default     |
| ---------- | -------------------------------------------------------------------- | ------------- | ----------- |
| FROM       | Start of the temporal resolution.                                    | [0,1]         | 0           |
| TO         | End of the temporal resolution.                                      | [0,1]         | 1           |
| RESOLUTION | Resolution of the model (1 corresponds to an hourly model).          | [0,8760]      | 1           |
| NBREGIONS  | Number of model regions to be considered.                            | {1,2,...,inf} | 4           |

### Parameters for simple4pips.gms
| Parameter     | Description                                                       | Range         | Default     |
| ------------- | ----------------------------------------------------------------- | ------------- | ----------- |
| METHOD        | Specifies the method to apply. Per default the model is solved as standard LP. With _--METHOD=PIPS_, the model is generated in PIPS-IPM++ input format. || standard_lp |
| BLOCK         | Controls the generation of block files. Possible options: <br/><br/> **-2:** Generation of a single gdx file that contains the entire (annotated) model instance. <br/> **-1:** Sequential generation of all the small block files. <br/> **{0,1,...,n}:** Generation of a single small block file. This is mainly of interest for distributed model generation. | {-2,-1,0,1,...,n} | -2 |
| RBSIZE        | Total number of region blocks.                                    |               | NBREGIONS   |
| TBSIZE        | Total number of time blocks.                                      |               | 24          |
| KEEPVENAMES   | Determines whether the block gdx files are generated with full variable and equation names (1) or if those names are squeezed out (0). | {0,1} | 1 |
| KEEPUELS | Determines whether the block gdx files are generated with GAMS UELs (1) or not (0). | {0,1} | 1 |
| SUPPRESSDM | Determines whether at creation of gdx files an additional dictionary gdx file is created which allows to map original GAMS variale and equation names to variables and equations in the gdx file(s). | {0,1} | 1 |
|  SLICE |  This parameter is only relevant in the context of distributed model generation when BLOCK $\geq$ 0. It controls the degree of sliced data reading and sliced model annotation. | {0,1,2} | 0 |
| SCALING       | Determines if a scaled version of the model with better numerical properties should be generated (1) or not (0). | {0,1} | 0 |
| NOSLACK       | Determines whether the SLACK variables should be removed from the model (1) or not (0). | {0,1} | 0 |
| NBSHIFTS      | The number of target time periods to which demand satisfaction can be shifted. | {0,1,...,n} | 0 |
| SHIFTSTEPSIZE | The number of time periods demand satisfaction is shifted per shift step. | {1,2,...,n} | 2 |
| RUNPIPS       | If activated (1), uses gmschk tool to split the monolithic gdx file into the files for the individual blocks and runs mpirun to solve the problem with PIPS-IPM++ and loads the solution via execute_loadpoint. | {0,1} | 0 |

For a more comprehensive documentation of the parameters checkout the [best practice guide](https://gitlab.com/beam-me/bpg/-/blob/master/EnergySystemModel_SpeedUp_BestPracticGuide.pdf) (appendix _A.6 SIMPLE Model for PIPS-IPM - simple4pips.gms_).

## Building the annotated gdx file

By default the parameters are set up to solve the model as a standard LP. To use SIMPLE with the PIPS-IPM++ solver some
additional steps are required. First we need to write out the problem matrix including the information on which 
variables and equations belong to which block. This can be achieved via the command line options:

```bash
gams simple4pips.gms --METHOD=PIPS --BLOCKS=-2
```

This call now generated a monolithic gdx file called *allblocks_noVEnames.gdx* containing the full annotated problem.

To check if the annotation was done correctly we provide the checkanno.gms tool which can be used by specifying the
gdx file we just created.

```bash
gams checkanno.gms --jacFileName=allblocks_noVEnames.gdx
```

The checkanno tool will create a new file called *allblocks_noVEnames_stats.csv* which reports some annotation 
statistics of the instance such as the number of blocks, if the annotation was correct, and if the number of linking
elements are small enought for PIPS-IPM++ to handle them.


## Solving the problem with PIPS-IPM++

If you have PIPS-IPM++ and the GAMS/PIPS-IPM++-Link installed you can split the monolithic gdx file into the files for 
the individual blocks by using the gmschk tool. Since we have indivicual 365 blocks in the annotated problem we need to 
specify 366 blocks to account for the linking block *allblocks_noVEnames0.gdx*. Additionally we need to specify the
directory in which GAMS is installed (here referenced as a environment variable GAMS_DIR)


```bash
gmschk -s -T -w -X -g $GAMS_DIR 366 allblocks_noVEnames.gdx
```

This created the distributed gdx files *allblocks_noVEnames[0-365].gdx*. To have PIPS-IPM++ solve the problem we need to 
specify the number of blocks as well as the stem of the filename in a MPI environment with MPI processes equal to
the number of individual blocks without the linking block.

```bash
mpirun -n 365 gmspips 366 allblocks_noVEnames $GAMS_DIR scaleGeo presolve stepLp
```

The following figure presents the general workflow of solving optimimization problems with PIPS-IPM++ compared to
solving them with non parallel algorithms.

![Workflow for the PIPS-IPM++ solver](fig/workflow.png?raw=true "Title")


## Block-wise generation of the gdx files

The same imput for the gmspips call can be generated in a parallel way by having simple4pips create the individual
blocks either sequentially or in parallel. The sequential generation can be achieved by teh following call 

```bash
gams simple4pips.gms --METHOD=PIPS --BLOCKS=-1
```

Similarly the individual blocks can be speficied by using the integer value for the corresponding block. This also 
allows the parallel generation of blocks by setting the blocks argument to the MPI rank.

```bash
gams simple4pips.gms --METHOD=PIPS --BLOCKS=[0-365]
```


## Authors
- Frederik Fiand (GAMS Software GmbH)
- Manuel Wetzel (German Aerospace Center)
- Michael Bussieck (GAMS Software GmbH)


## Acknowledgements
This research is part of the BEAM-ME project. It was funded by the German Federal Ministry for Economic Affairs and 
Energy under grant number FKZ 03ET4023A.